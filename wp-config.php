<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'shoppingCart' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Sh0P@1@#$' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5FUrqv(}N<~y4Uz.UrnU,+j@?e>v%6NIcL,TQLzG.>pYY}eU^MLk8pX&jKuvX>2~' );
define( 'SECURE_AUTH_KEY',  'j]<&a**=vWNt+U1w!uQB_zlba2O7h>YgOSYXOsXDvFlUJ%RyHn*.rJvXh9(PR]6^' );
define( 'LOGGED_IN_KEY',    'cBEzeFj:sTa[DjegB*y}EurO_,b-%Dw_>I~1ZKT#VE[hC)f`{C|azeR-9xWVZ_v/' );
define( 'NONCE_KEY',        'b@(/=l/.2Xy>NuUdH!j}|t@qUi[O-)<#BU6R(VSp`;!cQu72%(Fex%l6tgG$f4T>' );
define( 'AUTH_SALT',        '.Ak!<*)E(CZ<6<7Q~&&lf Wpyf%!VXi5oZ AdivDuo[7_dLoyzy5fSdEe_bZI(]$' );
define( 'SECURE_AUTH_SALT', 'f~!)+ Tv zWAWH)H}J&ru[LURucPSoLZ+{EMTfk/m-jkIV~RpDNpyeVc?&uMY7I:' );
define( 'LOGGED_IN_SALT',   '=xcI5lh|XcBvu~Lf=>YE|F~WqOlS-=7!Oa,w$ZHVwNQs=IdKn1:E$|d?EL2N3j*[' );
define( 'NONCE_SALT',       'V*<o*RwMd_rbD5?[K9bhIPO7-nN-^C>vUZPQ/!AaZ4wZ8F|c}C|VL|_1!BN5XOcK' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'shopoc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
